## This is a custom QGIS plugin requested from a client while freelancing

# _The plugin's functions_

1. Populate the **"Address"** field of the **"Nodes"** layer using the address data in the **"Customer Premise"** layer. _(excluding NULL vales)_  

2. Populate the **"Premises"** field of the **"Nodes"** layer by copying the value in the **"Premises"** field of the **"Boundaries"** layer  

# Example area with Customer Premises, Boundaries and Nodes

This is an example in image format to better understand the plugin's functions    
   
   
  

![](example.png)  
